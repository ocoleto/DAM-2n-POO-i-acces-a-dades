## M3 - Programació

### UF 1: programació estructurada.

### UF 2: disseny modular.

### UF 3: fonaments de gestió de fitxers.

### UF 4: programació orientada a objectes (POO). Fonaments.

### UF 5: POO. Llibreries de classes fonamentals.

 1. [Utilització avançada de classes](docs/utilitzacio_avancada_de_classes/README.md)
 * [Estructures d'emmagatzematge](docs/estructures_emmagatzematge/README.md)
 * [Interfícies gràfiques](docs/interficies_grafiques/README.md)

### UF 6: POO. Introducció a la persistència en BD.

## M5 - Entorns de desenvolupament

### UF 1: desenvolupament de programari.

### UF 2: optimització de programari.

### UF 3: introducció al disseny orientat a objectes.

 1. [Diagrames UML](docs/diagrames_uml/README.md)

## M6 - Accés a dades

### UF 1: persistència en fitxers.

### UF 2: persistència en BDR-BDOR-BDOO.

### UF 3: persistència en BD natives XML.

### UF 4: components d’accés a dades.
