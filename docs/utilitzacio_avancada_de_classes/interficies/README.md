### Interfícies

Una **interfície** és similar a una classe, però no té codi que la
implementi, només les capçaleres dels mètodes i variables estàtiques
constants (*static final*). Una classe pot declarar que *implementa* una
interfície, i llavors cal omplir el codi que manca a aquesta classe (o
bé declarar els mètodes abstractes i obligar a que sigui una classe
derivada qui ho faci, com veurem més endavant).

***Les interfícies ens permeten garantir que una determinada classe
compleix un determinat contracte***, és a dir, que inclou alguns mètodes
concrets.

A més, les interfícies són una versió fluixa de l'herència múltiple, que
no existeix en Java. Es poden utilitzar quan volem donar
característiques comunes a diverses classes que no estan relacionades
entre elles per l'herència.

Una interfície pot ampliar una altra interfície, creant una estructura
similar a l'herència de classes. Per altra banda, una interfície pot no
contenir cap mètode, i llavors només serveix per indicar que una classe
compleix una determinada propietat.

Hem vist diverses interfícies al tema d'estructures dinàmiques: *List*,
*Collection*, *Iterable*, *Iterator*...

Anem a estudiar l'ús de les interfícies amb alguns exemples extrets de
les API del Java.

 * [La interfície Comparable](comparable.md)
 * [La interfície Cloneable](cloneable.md)
 * [Exercicis](exercicis.md)
