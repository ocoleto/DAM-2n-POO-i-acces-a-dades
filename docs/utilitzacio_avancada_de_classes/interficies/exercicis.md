#### Exercicis

1. Creeu la classe *Persona* amb els atributs pes, edat i alçada. Implementeu
la interfície *Comparable* per fer que es compari l'alçada. Creeu un
*Comparator* per permetre que dues persones es puguin comparar per l'edat, i un
altre per tal que es puguin comparar pel pes.

2. La interfície *Serie* representarà una sèrie de nombres, de manera que, a
partir d'un nombre inicial es generarà la resta de la sèrie.

  * Crea aquesta interfície, amb els mètodes adequats per donar el nombre
  inicial, per inicialitzar-la a uns valors per defecte, i per obtenir els
  següents nombres de la sèrie.

  * Crea la classe *PerDos*, que generi la sèrie resultant de multiplicar per
  dos el nombre anterior.

  * Crea la classe *NegEntreDos*, que generi la sèrie resultant de canviar el
  signe i dividir entre dos el nombre anterior.

  * Fes un mètode que rebi una sèrie qualsevol i un nombre, i que mostri els
  primers 20 nombres de la sèrie.

  * Exemplifica l'ús d'aquest mètode amb un programa.

3. En un joc, dos jugadors enfrontaran tirades de daus segons les següents
regles:

  * Cada jugador tirarà un cert nombre de daus.

  * Cada jugador, a més, tindrà un valor a l'atac i un valor a la defensa, que
  seran nombres enters.

  * Després que els dos jugadors tirin els daus, el dau més gran d'una tirada
  s'emparellarà amb el dau més gran de l'altra, i així successivament. Si un
  jugador tira més daus que l'altre, els daus sobrants s'emparellen amb valors
  de 0.

  * Per cada parella de daus, si la suma de l'atac d'un jugador amb el valor del
  seu dau és més gran que la suma de la defensa de l'altre més el seu dau, es
  suma un punt a aquest jugador.

  * El resultat final de l'enfrontament és la diferència entre els punts
  obtinguts pels dos jugadors.

  ***Exemple:***
  ```
  Jugador 1: Atac: 3, defensa 1, daus 5, tirada:  1,  1, 4, 4, 5
  Jugador 2: Atac: 2, defensa 2, daus 4, tirada: (0), 2, 4, 5, 5
  Punts pel primer jugador: primer dau (1+3>0+2), tercer dau (4+3>4+2) i cinquè dau (5+3>5+2). Total: 3 punts.
  Punts pel segon jugador: segon dau (2+2>1+1), tercer dau (4+2>4+1), quart dau (5+2>4+1) i cinquè dau (5+2>5+1). Total: 4 punts.
  Resultat de l'enfrontament: 3 – 4 = -1.
  ```

  * Crea la classe Tirada amb els atributs d'atac, defensa i una tirada de
  daus.

  * Crea un constructor per *Tirada* que rebi els valors de l'atac i la
  defensa i el nombre de daus a tirar, i que creï la tirada de daus
  corresponent (és a dir, tiri els daus).

  * Sobreescriu el mètode *toString*. Fes que retorni una cadena que mostri
  la tirada. Per exemple, per la primera tirada de l'exemple, retornaria:

  ```
  AT: 3  DEF: 1  Daus (5): 11445
  ```

  * Fes que la classe *Tirada* implementi la interfície *Comparable*. El
  mètode *compareTo* retornarà el resultat de l'enfrontament entre dues
  tirades.

  * Fes que la classe *Tirada* implementi la interfície *Cloneable*.

  * Crea la classe *ProvaTirada* amb un mètode *main* que demostri l'ús de
  la classe Tirada. Aquest mètode escriurà per pantalla alguna cosa
  similar a:

  ```
  AT: 3 DEF: 1 Daus (5): 11445
  AT: 2 DEF: 2 Daus (4): 2455
  Comparació de les dues tirades: -1
  Comparació d'una tirada amb una d'igual: 0
  ```

4. En aquest exercici, veurem com una classe que produeix esdeveniments (com un
botó quan l'usuari fa clic a sobre seu), pot notificar a altres objectes per tal
que aquests actuïn en conseqüència.

  * Crea la interfície *Listener* amb el mètode *notifyEvent*. Aquest mètode
  serà cridat cada vegada que es produeixi un esdeveniment per tots aquells
  objectes que estiguin esperant esdeveniments. El mètode rep un nombre enter,
  que representarà el tipus d'esdeveniment que es produeix.

  * Crea la classe *GuardaEsdeveniments*. Aquesta classe implementarà la
  interfície *Listener*, i cada vegada que rebi un esdeveniment el guardarà en
  una llista. Sobreescriu el mètode *toString* per tal que retorni una cadena
  amb el contingut de la llista.

  * Crea la classe *MostraEsdeveniments*. Aquesta classe implementarà la
  interfície *Listener*, i cada vegada que rebi un esdeveniment mostrarà per
  pantalla quin esdeveniment s'ha produït (és a dir, el seu nombre).

  * Crea la classe *ProdueixEsdeveniments*. Aquesta classe conté una llista de
  *Listener*.

  * Crea el mètode *addEventListener* a la classe *ProdueixEsdeveniments*.
  Aquest mètode rep un *Listener* i l'afegeix a la llista.

  * Crea el mètode *creaEsdeveniment* a la classe *ProdueixEsdeveniments*.
  Aquest mètode rep un nombre (l'esdeveniment) i crida al mètode *notifyEvent*
  de tots els *Listener* de la llista, passant aquest nombre com a paràmetre.

  * Crea el mètode *main* a la classe *Principal*. En aquest mètode, crea un
  objecte *GuardaEsdeveniments*, un objecte *MostraEsdeveniments* i un objecte
  *ProdueixEsdeveniments*. Afegeix els dos primers objectes com a *Listener* de
  l'objecte *ProdueixEsdeveniments* (utilitzant el mètode *addEventListener*).
  Crida diverses vegades el mètode *creaEsdeveniment* de l'objecte
  *ProdueixEsdeveniments*, passant diferents valors. Finalment, visualitza el
  contingut de l'objecte *GuardaEsdeveniments* per pantalla.

  El resultat d'executar el programa tindrà aquest aspecte:

  ```
  S'ha produït l'esdeveniment 3
  S'ha produït l'esdeveniment 6
  S'ha produït l'esdeveniment 2
  3 6 2
  ```

  Quin sentit té aquest exercici?

  Imaginem que estem implementant una interfície gràfica que conté una
  llista amb una sèrie d'elements seleccionables. Quan l'usuari selecciona
  un element, volem que es mostri la informació d'aquest element a un
  quadre de text, i volem que algunes opcions del menú s'activin o es
  desactivin en funció de l'element seleccionat.

  Tot això ho volem fer mantenint la independència entre el quadre de
  text, el menú, i la llista. És a dir, no volem que des del codi de la
  llista es cridi directament a mètodes del quadre de text i del menú, Per
  què?

  1. Perquè els responsables de saber com s'han de configurar són el propi menú
  i el propi quadre de text, no la llista, que no té per que conèixer els
  detalls interns dels altres controls.

  2. Perquè volem reutilitzar la nostra llista a altres diàlegs, que potser no
  tindran menú o quadre de text.

  3. Perquè en algun moment potser voldrem afegir algun altre control que hagi
  de reaccionar als esdeveniments de la llista, i quan això passi, no volem
  haver de modificar el codi de la llista.

  Per resoldre aquest problema procedim com a l'exercici: la llista fa el
  paper de *ProdueixEsdeveniments* (és la font dels esdeveniments, que es
  produeixen quan l'usuari selecciona un element). La llista desplegable
  contindrà una llista amb tots els seus *Listener*: el menú i el quadre
  de text. Però tal com està dissenyat, no sabrà qui són i no els tindrà
  codificats directament. Només sabrà que implementen la interfície
  *Listener*, i el codi d'inicialització del diàleg serà qui haurà fet
  l'associació a través de la crida del mètode *addEventListener* de la
  llista desplegable (això té sentit, perquè el diàleg sí que sap quins
  elements ha de contenir i com es relacionen entre ells). Com s'ha vist,
  tant el menú com el quadre de text han d'implementar la interfície
  *Listener*: fan el paper dels *MostraEsdeveniments* i
  *GuardaEsdeveniments* de l'exercici.

  De tot això se n'anomena **Observer Pattern**.

  Diagrama de classes de l'exercici:

  ![diagrama de classes](imatges/exercici_interficies_observer_pattern.png)

  Diagrama de seqüència de l'exercici:

  ![diagrama de seqüència](imatges/exercici_interficies_observer_pattern_sequencia.png)

  Diagrama de classes de l'exemple de la llista en entorn gràfic:

  ![diagrama exemple GUI](imatges/exercici_interficies_observer_pattern_gui.png)
