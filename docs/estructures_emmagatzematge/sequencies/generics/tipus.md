##### El problema dels tipus

Tal com hem creat la llista a l'apartat anterior, a la nostra llista
s'hi poden posar referències a qualsevol tipus d'objecte. No s'hi poden
posar tipus primitius com *int* o *double*, però gràcies a les classes
equivalents, *Integer* i *Double*, i a la conversió automàtica entre els
primers i les segones (*autoboxing*), això no és cap inconvenient.

El que sí que és problemàtic és el fet que internament s'estiguin
tractant totes les referències als objectes de la llista com a tipus
*Object* (el tipus més genèric possible). Amb un exemple de codi es veu
clarament perquè això és molest:

```java
public class ProvaLlistes {
   public static void main(String[] args) {
       // Creem una llista buida
       List llista = new ArrayList();
       // Creem una cadena d'entrada
       String sin = "prova llistes";
       // Afegim la cadena a la llista
       llista.add(sin);
       // Traiem la cadena de la llista
       String sout = llista.get(0);
       // ERROR: l'element de la llista és de tipus Object!
   }
}
```

L'última línia dóna error de compilació. Encara que l'objecte que estem
referenciant és realment un *String*, si s'ha guardat com a tipus
*Object*, el compilador no té forma de saber que realment és un
*String*.

Fixeu-vos que assignar un objecte de tipus *String* a una referència de
tipus *Object* és correcte, perquè *String* deriva d'*Object*. És com
dir que ens mirarem un *Gos* com a un *Animal* qualsevol. Però no podem
agafar un *Animal* que tinguem i suposar automàticament que es tracta
d'un *Gos*, com tampoc podem agafar un *Object* i suposar que es tracta
d'un *String*.

Però nosaltres sí que estem segurs que l'objecte en qüestió és un
*String*! Aleshores, li podem dir al compilador que ja sabem què estem
fent, i que deixi de queixar-se. Això es fa amb l'anomenat **cast**:

```java
// Traiem la cadena de la llista
String sout = (String) llista.get(0);
```

En el nostre petit programa això és suficient, però què passa si en un
programa més gran i complex hem afegit una referència a la llista que no
és de tipus *String*?

```java
Integer iin = 3;
llista.add(iin);
String sout = (String) llista.get(0);
```

El compilador no es queixa, perquè li estem dient que confiï en
nosaltres i que converteixi la referència d'*Object* a *String*. Però
quan executem:

```java
Exception in thread "main" java.lang.ClassCastException: java.lang.Integer cannot be cast to java.lang.String
   at piles.ProvaLlistes.main(ProvaLlistes.java:19)
```

El programa “peta” en temps d'execució, amb una excepció de tipus
*ClassCastException*: hem intentat convertir quelcom que realment era un
*Integer* a un *String*.

![Relació entre String i Object](../../imatges/string_object.png)

Tenim diverses maneres de gestionar aquesta possibilitat. O abans de fer
la conversió ens assegurem que aquesta és correcte:

```java
String sout;
Object oout = llista.get(0);
if (oout instanceof String) {
    sout = (String) oout;
    // fer el que sigui amb sout
} else {
    // Codi per gestionar l'error
}
```

O bé capturem l'excepció i actuem en conseqüència:

```java
String sout;
try {
    sout = (String) llista.get(0);
    // fer el que sigui amb sout
} catch (ClassCastException e) {
    // Codi per gestionar l'error
}
```

Fixeu-vos com en el primer cas hem utilitzat l'operador **instanceof** per
comprovar si un objecte és d'un tipus determinat.

Amb això resolem les possibles petades del programa, però no és gratuït:
per una banda, seguim tenint el problema que en algun punt del programa
es pugui introduir a la llista un objecte de tipus incorrecte, i aquest
error només es detecta quan l'objecte s'extreu de la llista, de manera
que ens pot costar bastant trobar l'origen de l'error. Per altra banda,
el codi, que era molt senzill a la primera versió de l'exemple,
s'allarga i es complica amb el tractament de les situacions anòmales, i
es fa menys llegible.

De tot això n'hem de treure dues importants lliçons:

 * ***Els elements que introduïm en una llista han de ser del tipus més
 restringit possible***: és una mala idea utilitzar una mateixa llista per
 guardar objectes de tipus molt diferent.

 * ***Cal reservar els cast per aquelles poques situacions en què són
 imprescindibles***, i evitar-los en la resta de casos.
