#### Iteradors

D'entre els mètodes de *Collection* i *List*, el mètode *iterator()*
mereix especial atenció. Imaginem que hem implementat la llista en dues
classes, una com a vector dinàmic i l'altre com a llista enllaçada. Una
de les operacions habituals és recórrer tots els elements de la llista.
En el cas del vector dinàmic, per fer aquest recorregut necessitem un
índex, la posició del vector, que és un nombre enter, i podem accedir
amb temps constant a qualsevol posició. En canvi, en la llista enllaçada
l'índex es converteix en una referència a un *NodeLlista*, i és
important recordar el punt on ens trobem, perquè accedir al següent
element és ràpid, però accedir directament a un element intermedi del
qual només sabem que ocupa la posició 100, per exemple, és lent.

Així, per poder recórrer la llista hem de conèixer la seva implementació
interna, i hem d'accedir a una classe que en principi estava oculta com
el *NodeLlista*. Fent-ho d'aquesta manera, destruïm l'intent
d'encapsulació que hem fet amb les piles i cues, i ja no serà possible
escriure codi que sigui independent del tipus d'implementació que
s'estigui utilitzant.

Per evitar això, a les biblioteques del Java s'ha creat la interfície
*Iterator*. Quan vulguem recórrer una llista implementada com un vector,
l'*Iterator* encapsularà un enter, que guardarà la posició de l'últim
element visitat. Quan recorrem una llista enllaçada, l'*Iterator*
encapsularà una referència al *NodeLlista* que s'hagi visitat. En el
nostre programa, no accedirem directament ni a l'índex ni al
*NodeLlista*, perquè ho farem sempre a través dels mètodes descrits a la
interfície *Iterator*.

Realment, existiran dues classes que implementen *Iterator*, una per als
*ArrayList* (diem-li *ArrayListIterator*) i una altra per a les
*LinkedList* (diem-li *LinkedListIterator*). Però gràcies al
polimorfisme, de la mateixa manera que treballàvem amb una *Pila* sense
saber quin tipus de pila era, no sabrem de quina classe en concret és el
nostre *Iterator*, només que és un *Iterator*. És per això que m'he
inventat els noms de les classes dels iteradors, perquè realment no són
classes públiques i per saber-los s'hauria d'anar a cercar en el codi
font del Java.

Queda una qüestió per resoldre. Quan hem implementat les piles, només hi
havia un punt en què s'havia de conèixer amb quin tipus de pila volíem
treballar i era en el moment de crear l'objecte. Per evitar això, i per
evitar que puguem crear un *LinkedListIterator* per intentar iterar
sobre un vector dinàmic, els constructors d'aquestes classes tampoc són
públics. Com creo doncs un iterador per recórrer una llista? Doncs això
és justament el que fa el mètode *iterator()* declarat a la interfície
*Collection* (de fet, està declarat a *Iterable* i *Collection* amplia
*Iterable*).

Resumint, el mètode *iterator()* crea un *Iterator* adequat per poder
recórrer els elements d'una col·lecció.

**Nota**: s'aconsella no utilitzar la interfície *Enumerator*.

Fixeu-vos que un *Iterator* és similar al concepte de cursor en
altres llenguatges, com el PL/SQL o el PHP.

![Iterator pattern](../../imatges/iterator_pattern.png)

Els mètodes com *iterator()*, que tenen com a objectiu construir un
objecte adequat a la situació, sense que el programa que ho utilitza (el
*Client*), hagi de saber abans el tipus adequat, s'anomenen *mètodes
fàbrica* (**Factory Method**). Fixeu-vos que són les classes que implementen
*Iterable* les que decideixen de quin tipus serà l'iterador, en comptes
de decidir-se directament a la interfície *Iterable*, o a *Client*.

L'esquema sencer utilitzat per amagar la implementació interna dels
iteradors, i que es mostra a l'anterior diagrama UML, s'anomena *patró
iterador* (**Iterator pattern**).

Vegem els avantatges d'utilitzar un iterador amb alguns exemples de
codi. El següent mètode rep una llista d'enters i troba l'enter més gran
que hi ha:

```java
public static int max(List<Integer> llista) {
    int i;
    int maxim = 0;

    if (llista.size() == 0) {
        throw new NoSuchElementException();
    }

    maxim = llista.get(0);

    for (i=1; i<llista.size(); i++) {
        if (llista.get(i) > maxim)
            maxim = llista.get(i);
    }

    return maxim;
}
```

Fixeu-vos que ens interessa que el mètode rebi *List* en comptes
d'*ArrayList* o *LinkedList*: d'aquesta manera es pot utilitzar el
mateix mètode per trobar el màxim dels dos tipus de llistes.

El problema és que si es tracta d'una *LinkedList*, i té molts elements,
amb el mètode *get* tardarem molt a iterar, perquè cada vegada es
recorreran tots els elements fins a trobar l'element cercat.

Podem canviar el nostre mètode per tal que utilitzi iteradors:

```java
public static int max(List<Integer> llista) {
    Iterator<Integer> it;
    int maxim = 0;
    int n;

    if (llista.size() == 0) {
        throw new NoSuchElementException();
    }

    maxim = llista.get(0);
    it = llista.iterator();
    while (it.hasNext()) {
        n = it.next();
        if (n > maxim)
            maxim = n;
    }

    return maxim;
}
```

Aquest mètode fa el mateix que l'anterior, però gràcies a l'ús de
l'iterador, serà tant ràpid sobre un *ArrayList* com sobre un
*LinkedList*. Fixeu-vos com els iteradors també es paramatritzen amb el
tipus de dada que retornaran, utilitzant els genèrics.

És important notar que la crida al mètode *next()* ens retorna l'element
següent i fa avançar l'iterador a la següent posició. Una sentència com:

```java
if (it.next() > maxim)
    maxim = it.next();
```

seria incorrecta, perquè estem avançant dues vegades l'iterador en
comptes d'una, i l'element que ens retorna a la primera i segona crida
del *next* és diferent. Per això he utilitzat una variable *n* auxiliar
per emmagatzemar el valor de l'element, i així només cridar a *next* una
vegada.

A més, el podem generalitzar encara una mica més. En el següent exemple
hem eliminat les crides a *size* i *get*, i ho fem tot a través de
l'iterador. Per tant, no cal que suposem que ens passen una llista, sinó
que podem utilitzar-lo sobre qualsevol col·lecció, com per exemple, un
conjunt (*Set*):

```java
public static int max(Collection<Integer> iterable) {
    Iterator<Integer> it;
    int maxim = 0;
    int n;

    it = iterable.iterator();
    if (it.hasNext())
        maxim = it.next();
    else
        throw new NoSuchElementException();

    while (it.hasNext()) {
        n = it.next();
        if (n > maxim)
            maxim = n;
    }

    return maxim;
}
```

 * [Els iteradors de llistes: *ListIterator*](listiterator.md)
 * [Inserir i extreure elements a través d'un iterador](insert_remove.md)
 * [Ús d'un iterador sense l'iterador: enhanced for](enhanced_for.md)
